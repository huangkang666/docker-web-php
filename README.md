说明
===============

### php 环境 docker版

##################### 端口与版本说明 ####################

|               | 版本          | 端口            |    
| ------        | ------        | -----         |
|   php-apache  |    v7.4       | 8080          |
|    mysql      |     v5.7      |    3307       |
|    phpmyadmin |               |    8886       |
|    Redis      |    v5         |    6379       |


##################### docker 操作 #######################

### 1. 文件位置
将文件夹放到与 项目同级

修改 docker-compose.yaml 文件中 # 项目目录  

### 2. shell 运行

cd 到当前文件夹

执行 
```
# 构建（容器）并启动（容器）所有service
docker-compose up -d 

# 查看启动的service列表
docker-compose ps

# 测试环境
打开浏览器 运行  查看php环境
127.0.0.1:{$post}/public/

打开浏览器运行 查看phpmyadmin 
127.0.0.1:{$post}
```

